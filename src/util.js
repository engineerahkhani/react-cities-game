export function distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
        dist = 1;
    }
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") {
        dist = dist * 1.609344
    }
    if (unit == "N") {
        dist = dist * 0.8684
    }
    return dist
}

export const citeis = [
    {
        id: 1,
        name: 'London',
        cityLat: 51.5,
        cityLng: -0.083333,
    },
    {
        id: 2,
        name: 'Berlin',
        cityLat: 52.51666667,
        cityLng: 13.4,
    },
    {
        id: 3,
        name: 'Stockholm',
        cityLat: 59.33333333,
        cityLng: 18.05,
    }, {
        id: 4,
        name: 'Paris',
        cityLat: 48.86666667,
        cityLng: 2.333333,
    }, {
        id: 5,
        name: 'Reykjavik',
        cityLat: 64.15,
        cityLng: -21.95,
    },
    {
        id: 6,
        name: 'Rome',
        cityLat: 41.9,
        cityLng: 12.483333,
    },    {
        id: 7,
        name: 'Lisbon',
        cityLat: 38.71666667,
        cityLng: -9.133333	,
    },
]