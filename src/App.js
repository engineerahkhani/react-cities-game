import React, {Component} from 'react';
import {withGoogleMap, GoogleMap, Marker, InfoWindow} from "react-google-maps";
import {distance, citeis} from './util';

const height = window.innerHeight;

const initState = {
    flagPosition: {lat: 51.5, lng: -0.083333},
    score: 1500,
    foundedCities: 0,
    activeIndex: 0,
    gameOver: false,
    finish: false,
}
const Label = ({title, value}) => <div style={{
    display: 'flex', flexDirection: 'row',backgroundColor:'#dddddf',border: '1px solid #000',marginTop:'2px',
    justifyContent:'center',alignItems:'center'

}}>
    <span style={{color:"#000"}}>{value}</span>
    <span style={{color:'#000'}}>{title}</span>
</div>;

const Win = (props) => <div className='alert alert-success'>
    <div>You Win :)</div>
    <button className='btn btn-primary' onClick={() => props.palyAgain()}>play again</button>

</div>;
const GameOver = (props) => <div className='alert alert-warning'>
    <div>Game Over :(</div>
    <button className='btn btn-primary' onClick={() => props.palyAgain()}>play again</button>

</div>;


const CustomMap = withGoogleMap(props => (
    <GoogleMap
        defaultZoom={5}
        defaultCenter={{lat: 51.5, lng: -0.083333,}}
        defaultOptions={{
            styles: [ { featureType: "administrative.country", elementType: "labels", stylers: [ { visibility: "off" } ] },
                { featureType: "administrative.city", elementType: "labels", stylers: [ { visibility: "off" } ] }
            ],
            // these following 7 options turn certain controls off see link below
            streetViewControl: false,
            scaleControl: false,
            mapTypeControl: false,
            panControl: false,
            zoomControl: false,
            rotateControl: false,
            fullscreenControl: false,
            scrollwheel: false
        }}
        style={[{
            featureType: "administrative.country",
            elementType: "labels",
            stylers: [
                { visibility: "off" }
            ]
        }]}

    >
        <Marker
            draggable={true}
            onDragEnd={(e) => props.onDragEnd(e.latLng.lat(), e.latLng.lng())}
            position={{...props.flagPosition}}
        />
    </GoogleMap>
));

class App extends Component {
    state = {

        ...initState,
    };

    onDragEnd = (lat, lng) => {
        this.setState({
            flagPosition: {lat, lng}
        })
    };
    handleBtnPress = () => {
        const {flagPosition, activeIndex} = this.state;
        const {lat, lng} = flagPosition;
        const {cityLat, cityLng} = citeis[activeIndex];
        const dis = distance(lat, lng, cityLat, cityLng, 'K');
        if (parseInt(dis, 10) <= 50) {
            this.setState(state => ({foundedCities: state.foundedCities + 1}))
        }
        this.setState(state => {
            let result = {};
            const newScore = state.score - parseInt(dis, 10);
            if (newScore < 0) {
                result = {gameOver: true}
            }
            if (state.activeIndex === citeis.length - 1) {
                result = {finish: true}
            }

            return {
                ...result,
                activeIndex: state.activeIndex + 1,
                score: newScore
            };
        });



        console.log(dis)
    }
    palyAgain = () => this.setState({...initState})
    showMessage = () => {
        const {flagPosition, score, foundedCities, activeIndex, finish, gameOver} = this.state;
        if (gameOver) {
            return <GameOver palyAgain={() => this.palyAgain()}/>
        }
        if (finish) {
            return <Win palyAgain={() => this.palyAgain()}/>
        }
        return <div>
            <header className='max-height' style={{justifyContent:'center',alignItems:'center',paddingTop:'30px'}}>
                <Label title='cities placed' value={foundedCities}/>
                <Label title='kilometers left placed' value={score}/>
                <div style={{textAlign:'center'}}>select location of</div>
                <div style={{textAlign:'center'}}> {citeis[activeIndex].name} </div>
            </header>
            <CustomMap
                containerElement={<div className='height'/>}
                mapElement={<div className='height'/>}
                onDragEnd={(lat, lng) => this.onDragEnd(lat, lng)}
                flagPosition={flagPosition}
            />
            <div  style={{
                display:'flex',
                justifyContent:'flex-end'
            }}>
                <button style={{
                    backgroundColor:'#dddddf',padding:'5px 30px',
                    border: '1px solid #000',marginTop:'2px'
                }} onClick={() => this.handleBtnPress()}>
                    Place
                </button>
            </div>
        </div>
    }

    render() {

        return (
            <div className='container-fluid'>
                <div className='row'>
                    <div className='col-sm-12 col-sm-6  '>
                        {
                            this.showMessage()
                        }
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
